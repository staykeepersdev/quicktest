# QUICK LIVE TEST PROJECT -> Django & Django REST framework
The goal of this test is just to show some basic Django skills in a quick live coding session.

This repo has only the minimum requirements necessary to spin a new Django platform.

_**NOTE:** This test project won't be used in any real environment._

## Initial project steps

- Clone the repo
- Create and run the Virtual Enviornment
- Install project dependencies
- Create main Django project folder and run migrations
- Create Admin Django superuser - _with password_
- Create the Test App django application
- Run local Django

## Setup commands

    git clone https://bitbucket.org/staykeepersdev/quicktest.git
    python -m venv venv
    source venv/bin/activate or source venv/Scripts/activate
    pip install -r requirements.txt
    django-admin startproject quicktest .
    python manage.py migrate
    python manage.py createsuperuser --username admin
    python manage.py startapp testapp
    python manage.py runserver

## Summary:

**Part 1:** Create 4 models in the Test App:

- Building
    - Name - CharField

- Room Type
    - Name - CharField
    - Type - ChoiceField('Single', 'Double')
    - Building - Foreign Key /required/

- Room
    - Number - Positive Small Int Field
    - Room Type - Foreign Key /required/

- BlockedDay
    - Day - DateField()
    - Room - Foreign Key /required/

Make sure the models above are linked correctly with Foreign Keys.

Make & apply migrations.

----

**Part 2:** Fill the Test App models with test data

1. Run Django shell or shell_plus.

2. Create the following objects:

    - 1 Building
    - 1 Room Type
    - 1 Room
    - 20 Blocked Days
        - 01 Dec 2021 - 10 Dec 2021
        - 20 Dec 2021 - 30 Dec 2021

3. Exit shell / shell_plus

----

**Part 3:** Create Django Rest endpoint

1. Create endpoint that displays all **Free & Blocked days**

For a GET request with the following query params:

GET /free-blocked-days/1/1/1/2021/12

- **Building** with ID X
- **Room Type** with ID Y
- **Room** with ID Z
- **For Month:** December 2021

This will require URL linking / View file.

A serializer implementation is optional for this quick task.